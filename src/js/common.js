$(".explore-zone .owl-carousel").owlCarousel({
	nav: true,
	navText: ['',''],
	dots: false,
	margin: 10,
	slideBy: 6,
	rewind: true,
	responsive:{
		0:{
			 items: 1
		},
		700:{
			 items: 2
		},
		1000:{
			 items: 3
		},
		1200:{
			items: 4
		},
		1400: {
			items: 5
		},
		1700:{
			items: 6
		}
  }
});

$(".what-section .owl-carousel").owlCarousel({
	nav: true,
	navText: ['',''],
	dots: false,
	margin: 30,
	slideBy: 3,
	rewind: true,
	responsive:{
		0:{
			 items:1
		},
		700:{
			 items:2
		},
		1000:{
			 items:3
		}
  }
});

$(".product-auction-section .slider.owl-carousel").owlCarousel({
	items: 1,
	nav: true,
	navText: ['',''],
	dots: false,
	margin: 0,
	rewind: true
});
